package org.openweathermap.data.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherData {
    private Long id;
    private String name;
    private Integer cod;
    private String base;
    private Long dt;
    private Integer timezone;
    private Integer visibility;
    private Coord coord;
    private List<Weather> weather;
    private Main main;
    private Wind wind;
    private Clouds clouds;
    private Rain rain;
    private Snow snow;
    private Sys sys;

    @Data
    public static class Coord {
        private Float lon;
        private Float lat;
    }

    @Data
    public static class Weather {
        private Long id;
        private String main;
        private String description;
        private String icon;
    }

    @Data
    public static class Main {
        @JsonProperty("temp")
        private Float temperature;
        private Float pressure;
        private Integer humidity;
        @JsonProperty("temp_min")
        private Float temperatureMin;
        @JsonProperty("temp_max")
        private Float temperatureMax;
        @JsonProperty("sea_level")
        private Float seaLevel;
        @JsonProperty("grnd_level")
        private Float grndLevel;
    }

    @Data
    public static class Wind {
        private Float speed;
        @JsonProperty("deg")
        private Float degree;
        private Float gust;
    }

    @Data
    public static class Clouds {
        private Integer all;
    }

    @Data
    public static class Rain {
        @JsonProperty("1h")
        private Float rainOneHour;
        @JsonProperty("3h")
        private Float rainThreeHours;
    }

    @Data
    public static class Snow {
        @JsonProperty("1h")
        private Float snowOneHour;
        @JsonProperty("3h")
        private Float snowThreeHours;
    }

    @Data
    public static class Sys {
        private Long id;
        private Integer type;
        private Float message;
        private String country;
        private Long sunrise;
        private Long sunset;
    }
}
